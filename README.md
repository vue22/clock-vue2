# vue-clock

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

## Описание
### Функционал
Часы с возможностью настройки размеров их составляющих, изменения видимости части элементов часов.

##Plugins
vue-range-slider(npm install --save vue-range-slider) - custom slider, based on default input[type="range"]
